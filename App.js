import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
} from 'react-native';

class App extends Component {
  state = {
    height: '',
    weight: '',
    BmiIndicator: '',
    BmiResult: '',
  };
  handleHeight = text => {
    this.setState({height: text});
  };
  handleWeight = text => {
    this.setState({weight: text});
  };
  calculate = (height, weight) => {
    //calculation
    var result =
      (parseFloat(weight) * 10000) / (parseFloat(height) * parseFloat(height));
    result = result.toFixed(2);
    //display result
    this.setState({BmiIndicator: result});
    if (result < 18.5) {
      this.setState({BmiResult: 'Underweight'});
    } else if (result >= 18.5 && result < 24.9) {
      this.setState({BmiResult: 'Normal weight'});
    } else if (result >= 24.9 && result < 29.9) {
      this.setState({BmiResult: 'Overweight'});
    } else if (result >= 30.0 && result < 34.9) {
      this.setState({BmiResult: 'Obesity Class I'});
    } else if (result >= 35.0 && result < 39.9) {
      this.setState({BmiResult: 'Obesity Class II'});
    } else if (result >= 40) {
      this.setState({BmiResult: 'Obesity Class III'});
    } else {
      alert('Incorrect Input!');
      this.setState({BmiResult: ''});
    }
  };
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>BMI Calculator</Text>
        <Text style={styles.label}>Height</Text>
        <TextInput
          style={styles.input}
          underlineColorAndroid="transparent"
          placeholder="Height (Cm)"
          onChangeText={this.handleHeight}
          keyboardType="numeric"
        />
        <Text style={styles.label}>Weight</Text>
        <TextInput
          style={styles.input}
          underlineColorAndroid="transparent"
          placeholder="Weight (Kg)"
          onChangeText={this.handleWeight}
          keyboardType="numeric"
        />
        <TouchableOpacity
          style={styles.submitButton}
          onPress={() => this.calculate(this.state.height, this.state.weight)}>
          <Text style={styles.submitButtonText}> Calculate BMI </Text>
        </TouchableOpacity>
        <Text style={styles.output}>{this.state.BmiIndicator}</Text>
        <Text style={styles.resultText}>{this.state.BmiResult}</Text>
      </View>
    );
  }
}
export default App;
const styles = StyleSheet.create({
  container: {
    paddingTop: '50%',
  },
  input: {
    margin: 15,
    height: '10%',
    borderWidth: 1,
    padding: 10,
  },
  submitButton: {
    backgroundColor: '#6f5f5f',
    marginBottom: '10%',
    padding: 10,
    margin: 15,
    height: '10%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  submitButtonText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 18,
  },
  output: {
    textAlign: 'center',
    fontSize: 30,
  },
  title: {
    paddingBottom: '10%',
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#6f5f5f',
  },
  resultText: {
    paddingTop: 20,
    paddingBottom: 10,
    textAlign: 'center',
    fontSize: 30,
    color: '#808080',
  },
  label: {
    marginLeft: 15,
  },
});
